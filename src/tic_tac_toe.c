#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include "../include/server.h"
#include "../include/tic_tac_toe.h"

void * play_battleship(void *arg) {}


/**
* @brief thread handler that plays against the client
*
* Runs the game loop for each connected client. Gets the users
* tile selection and the AI's tile selection, and updates the
* board accordingly. Checks at the top of every loop if the
* game is done or not.
*
* @param void *arg      thread struct that gets passed in
* @return               None
*/
void* play_tic_tac_toe(void *arg)
{
    printf("hey we are playing now\n");
    struct thread *t = arg; // get thread struct 

    srand(time(NULL)); //seed AI's guessing prng
    char gameboard[3][3] = {
        {'1', '2', '3'},
        {'4', '5', '6'},
        {'7', '8', '9'}
    };

    // show the game board to the client
    print_board(t->client.sockfd, gameboard);
    char *user_tile = calloc(10, sizeof(char*));
    char *ai_tile = calloc(10, sizeof(char*));

    // game loop
    while(1) {
        // this is probably redundant now that we perform this check
        // in the user and AI tile selection functions, but ehh
        if( -1 == check_board(t->client.sockfd, gameboard)) {
            printf("Sorry folks, we're done here!\n");
            free(user_tile);
            free(ai_tile);
            free(t);
            return 0;
        }
        get_user_tile(t->client.sockfd, user_tile, gameboard);

        // figure out what tile the user want to play. We need to
        // do this check twice because we were lazy updating the board
        // and the AI won't know it's guess was the same as the users
        int i = 0;
        int k = 0;
        for(i = 0; i < 3; i++) {
            for (k = 0; k < 3; k++) {
                if(gameboard[i][k] == *user_tile) {
                    gameboard[i][k] = 'x';
                }
            }
        }
        get_ai_tile(t->client.sockfd, ai_tile, gameboard);
        for(i = 0; i < 3; i++) {
            for (k = 0; k < 3; k++) {
                if(gameboard[i][k] == *ai_tile) {
                    gameboard[i][k] = 'Y';
                }
            }
        }
        // show the updated board to the user
        print_board(t->client.sockfd, gameboard);
    }
}

/**
* @brief gets the users tile selection
*
* Recursive function that tries to get the users tile selection. If
* they choose a tile number outside the range (1-9), make them rechoose.
* This is done by calling the function recursively. If they choose a
* tile that is already taken, make them rechoose. Every time this
* happens make sure the game isn't over.
*
* @param sockfd             the socket for the client
* @param user_tile          string to hold the users tile selection
* @param gameboard          the current gameboard
* @return                   none
*/
void get_user_tile(int sockfd, char *user_tile, char (*gameboard)[3])
{
    int found = 0;
    unsigned char buf[1024] = { 0 }; // msg from user
    int n = 0; // holds size of received message from user
    static const char invalid_tile_msg[] = "Your number is too long, enter 1-9";
    // tell user to select tile
    write(sockfd, "select tile: ", strlen("select tile: ")+1);

    // get the users selection
    if((n = recv(sockfd, buf, sizeof buf, 0)) > 0) {
        buf[strlen(buf) -1] = '\0'; // strip newline 

        // if they give us more than 1 character long
        // (so 10+), make them guess again until they get it right
        if (n > 3) {
            write(sockfd, invalid_tile_msg, sizeof(invalid_tile_msg));
            return get_user_tile(sockfd, user_tile, gameboard);
        }

        // make sure they didn't select a taken tile
        int i;
        int k;
        for(i = 0; i < 3; i++) {
            for (k = 0; k < 3; k++) {
                if(gameboard[i][k] == buf[0]) {
                    found = 1;
                }
            }
        }
        static const char number_taken_msg[] = "Bad Player! That number is taken ";
        if(found == 0) {
            // make sure the game isn't out of moves or won by someone
            if( -1 == check_board(sockfd, gameboard)) {
                return;
            }
            write(sockfd, number_taken_msg, sizeof(number_taken_msg));
            return get_user_tile(sockfd, user_tile, gameboard);
        }
    }
    
    // copy the users good tile selection to the passed in var
    memcpy(user_tile, &buf[0], strlen(&buf[0]));
    return;
}


/**
* @brief get the AI's tile selection
*
* get a random number between 1 and 9. Make sure it's not taken
* already, if it is, call the function recursively till it's a
* clean guess
*
* @param sockfd             socket for client. Used for check_board
* @param ai_tile            var passed in to get AI's selection
* @param gameboard          the current gameboard
*/
void get_ai_tile(int sockfd, char *ai_tile, char (*gameboard)[3])
{
    int found = 0;
    int r = (rand() % 9) + 1; // 1 - 9
    // copy the random number to the pass through variable
    snprintf(ai_tile, 2, "%d", r);
    int i = 0;
    int k = 0;
    // if we've found it, it means the tile is already taken
    // and they need to rechoose
    for(i = 0; i < 3; i++) {
        for (k = 0; k < 3; k++) {
            if(gameboard[i][k] == *ai_tile) {
                found = 1;
            }
        }
    }
    if(found == 0) {
        if( -1 == check_board(sockfd, gameboard)) {
            return;
        }
        return get_ai_tile(sockfd, ai_tile, gameboard);
    }
    return;
}

/**
* @brief make sure there are still moves left
*
* first we check check_win() to make sure there isn't a winning play
* on the board. If not, iterate through the board and make sure there
* is still an empty tile that can be played
*
* @param sockfd             client socket for check_win()
* @param gameboard          current gameboard to check
* @return                   -1 if board is dead, 1 if still tiles
*/
int check_board(int sockfd, char (*gameboard)[3])
{
    check_win(sockfd, gameboard);
    int i = 0;
    int k = 0;
    for(i = 0; i < 3; i++) {
        for (k = 0; k < 3; k++) {
            // there's still a play to be made
            if(gameboard[i][k] >= '0' && gameboard[i][k] <= '9') {
                return 1;
            }
        }
    }
    return -1;
}

/**
* @brief check for win conditions
*
* @param sockfd             client socket so we can close it 
                            if there is a winning condition. This should
                            probably be moved out elsewhere but ehh
* @param gameboard          current gameboard
*/
int check_win(int sockfd, char (*gameboard)[3])
{
    int n = 0;
    char *msg = calloc(sizeof(char *), 1024);
    // 3 across any row
    int i = 0;
    for(i = 0; i < 3; i++) {
        if(
            (   
                'x' == gameboard[i][0] && 
                'x' == gameboard[i][1] &&
                'x' == gameboard[i][2]
            ) 
        ||
            (  
                'Y' == gameboard[i][0] && 
                'Y' == gameboard[i][1] &&
                'Y' == gameboard[i][2]
            )
        ) {
            n = snprintf(msg, sizeof(&gameboard[i][0])+strlen("WINNER: !\n"), "WINNER: %c!\n", gameboard[i][0]);
            write(sockfd, msg, n);
            close(sockfd);
        } 
    }

    // 3 down any row
    for(i = 0; i < 3; i++) {
        if(
            (
                'x' == gameboard[0][i] &&
                'x' == gameboard[1][i] &&
                'x' == gameboard[2][i]
            )
        ||
            (
                'Y' == gameboard[0][i] && 
                'Y' == gameboard[1][i] &&
                'Y' == gameboard[2][i]
            )
        ) {
            n = snprintf(msg, sizeof(&gameboard[0][i])+strlen("WINNER: !\n"), "WINNER: %c!\n", gameboard[0][i]);
            write(sockfd, msg, n);
            close(sockfd);
        }
    }
    // 3 diagonal down
    if(
        (
            'x' == gameboard[0][0] &&
            'x' == gameboard[1][1] &&
            'x' == gameboard[2][2]
        )
    ||
    (
            'Y' == gameboard[0][0] &&
            'Y' == gameboard[1][1] &&
            'Y' == gameboard[2][2]
    )
    ) {
            n = snprintf(msg, sizeof(&gameboard[0][0])+strlen("WINNER: !\n"), "WINNER: %c!\n", gameboard[0][0]);
            write(sockfd, msg, n);
            close(sockfd);
    }
    // 3 diagonal up
     if(
        (
            'x' == gameboard[2][2] &&
            'x' == gameboard[1][1] &&
            'x' == gameboard[0][0]
        )
    ||
    (
            'Y' == gameboard[2][2] &&
            'Y' == gameboard[1][1] &&
            'Y' == gameboard[0][0]
    )
    ) {
            n = snprintf(msg, strlen(&gameboard[0][0])+strlen("WINNER: !\n"), "WINNER: %c!\n", gameboard[0][0]);
            write(sockfd, msg, n);
            close(sockfd);
    }
    free(msg);
}

/**
* @brief sends the board to the client
*
* @param sockfd             the clients socket
* @param gameboard          the gameboard to show
* @return                   none
*/
void print_board(int sockfd, char (*gameboard)[3])
{
    int msg_size = 0;
    char *msg = calloc(sizeof(char *), 1024);
    int frame_size = strlen(" |  %c  | \n")+1;
    int i = 0;
    int k = 0;
    int cur_tile_size = 0;
    for(i = 0; i < 3; i++) {
        for (k = 0; k < 3; k++) {
            cur_tile_size = sizeof(&gameboard[i][k]);
            // if we have a middle tile, print boundaries
            if(k == 1) {
                msg_size = snprintf(msg, 
                                frame_size+cur_tile_size, 
                                " |  %c  | ", gameboard[i][k]);
                write(sockfd, msg, msg_size);
            } else {
                msg_size = snprintf(msg, 
                                (frame_size-4)+cur_tile_size, 
                                " %c ", gameboard[i][k]);
                write(sockfd, msg, msg_size);
            }
        }
        
        // print divider lines between rows 1 and 2
        if(i < 2) {
            write(sockfd, "\n---------------", 
                    strlen("\n---------------")+1);
        }
        write(sockfd, "\n", strlen("\n")+1);
    }
    free(msg);
}
