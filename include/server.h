#ifndef __SERVER_H__
#define __SERVER_H__

#include <sys/socket.h>
#include <netdb.h>
#include <netinet/in.h>
#include <unistd.h>
#include <errno.h>

#define MAX_CLIENTS 10

struct client {
    struct sockaddr_in client_ip;
    int sockfd;
    char username[25];
    int opponent; // 0 is AI, positive number is sockfd 
    int in_game;
};

struct server {
    struct sockaddr_in bind_ip;
    int sockfd;
    socklen_t bind_addr_sz;
};

struct thread {
    pthread_t pt;
    struct client client;
    volatile int  done;
};

int num_clients;
struct client cli_list[MAX_CLIENTS];

int socket_write(int sockfd, char *msg, int msg_size);
//uint16_t is 65535, max number of ports
int server_setup(uint16_t port);
int accept_client(struct client* client, struct server *server);

// strings
static const char welcome_msg[] = "Welcome to sean_ch0n's game server! \n What is your username? ";
static const char bad_username_msg[] = "Sorry, your username is bad, try again ";
static const char game_choice_msg[] = "Choose which game you would like to play: \n 1) tic tac toe against AI \n 2) battleship against AI\n 3) tic tac toe against player\n";
static const char bad_selectin_msg[] = "Bad selection, try again ";

#endif
