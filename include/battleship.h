#ifndef __SERVER_H__
#define __BATTLESHIP_H__

void* play_battleship(void *arg);
void print_battleship_board(char (*gameboard)[10]);

#endif
