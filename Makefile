src = $(wildcard src/*.c)
obj = $(src:.c=.o)

CFLAGS = -g
LDFLAGS = -g -Wall -pthread

myprog: $(obj)
	$(CC) -o $@ $^ $(LDFLAGS)

.PHONY: clean
clean:
	rm -f $(obj) myprog
