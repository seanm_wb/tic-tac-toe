#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include <pthread.h>
#include <signal.h>
#include "../include/tic_tac_toe.h"
#include "../include/server.h"

void intHandler(int dummy);
void* welcome_player(void *arg);
int find_opponent(struct client cli_list[MAX_CLIENTS], struct client *new_player, int num_clients);


int main(int argc, char *argv[])
{
    int err = 0;
    struct server s = { 0 }; // this is us
    struct client c = { 0 }; // this is any connected client
    uint16_t port = strtol(argv[1], NULL, 10);
    int sockfd = -1;
    
    // start the server listening. sockfd is the socket file descriptor
    // that we are listening on
    if((sockfd = server_setup(port)) < 1) {
        printf("[!] ERROR in server setup\n");
        return -1;
    }
    s.sockfd = sockfd;

    num_clients = 0;
    //cli_list = calloc(MAX_CLIENTS, sizeof(struct clients*));
    
    // loop forever trying to accept new clients and put them in a thread
    while (1) {
        signal(SIGINT, intHandler);
        if((err = accept_client(&c, &s))) {
            printf("[!] Failed to accept client: %s\n", strerror(errno));
            break;
        }
    
        struct thread *t = malloc(sizeof(struct thread));
        t->done = 0;
        t->client = c;
        // attempt to create the thread and put them in play_game()
        if(pthread_create(&t->pt, NULL, welcome_player, t) != 0) {
            printf("[!] Error creating thread\n");
        }
    }
    return 0;
}

void intHandler(int dummy)
{
    printf("[!] Ctrl+C caught, exiting...\n");
    exit(0);
}

void* welcome_player(void *arg)
{
    printf("Welcoming player...\n");
    struct thread *t = arg; // get the thread struct
    unsigned char buf[1024] = { 0 }; // msg from user
    int n = 0; // holds size of received message from user

    // add client to the client list and increment count
    memcpy(&cli_list[num_clients], &t->client, sizeof(t->client));
    num_clients++;

    // get the users username
    write(t->client.sockfd, welcome_msg, sizeof(welcome_msg));
    if((n = recv(t->client.sockfd, buf, sizeof buf, 0)) > 0) {
        buf[strlen(buf) -1] = '\0'; // strip newline

        // the max username length is 25
        if (n > 25) {
            write(t->client.sockfd, bad_username_msg, sizeof(bad_username_msg));
        }
        else {
            strncpy(t->client.username, buf, strlen(buf)+1);
            //memcpy(t->client.username, &buf, strlen(buf));
            printf("%s\n", t->client.username);
        }

        // get the users game choice
        write(t->client.sockfd, game_choice_msg, sizeof(game_choice_msg));
        if((n = recv(t->client.sockfd, buf, sizeof buf, 0)) > 0) {
            buf[strlen(buf) -1] = '\0';
            // play tic tac toe against AI
            if((0 == strncmp(buf, "1", 1))) {
                printf("%s\n", buf);
                t->client.opponent = 0;
                t->client.in_game = 1;
                play_tic_tac_toe(t);
            }
            // play battleship against AI
            else if((0 == strncmp(buf, "2", 1))) {
                printf("%s\n", buf);
                t->client.in_game = 1;
                play_battleship(t);
            }
            // play tic tac toe against player
            else if((0 == strncmp(buf, "3", 1))) {
                printf("%s\n", buf);
                t->client.opponent = -5;
                t->client.in_game = 0;
                find_opponent(cli_list, &t->client, num_clients);
                t->client.in_game = 1;
                play_tic_tac_toe(t);
            }
        }
   }
}

/*
 *  @brief find an opponent to play against
 *
 *  our cli_list struct holds a list of all the connected clients. Iterate
 *  through those to see which ones are not in a game (client.in_game == 0).
 *  Then set their opponent (client.opponent) to the sockfd of our new player.
 *  Set the new players sockfd to the sockfd of the opponent.
 */
int find_opponent(struct client cli_list[MAX_CLIENTS], struct client *new_player, int num_clients)
{
    int i = 0;
    for(i = 0; i < num_clients; i++) {
        if(cli_list[i].opponent == -5) {
            // we found a worthy opponent
            printf("WE FOUND AN OPPONENT\n");
            cli_list[i].opponent = new_player->sockfd;
            new_player->opponent = cli_list[i].sockfd;
            cli_list[i].in_game = 1;
            new_player->in_game = 1;
            return 0;
        }
    }

    return -1;
}
