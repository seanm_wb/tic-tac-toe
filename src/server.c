#include "../include/server.h"
#include <stdio.h>
#include <string.h>


int server_setup(uint16_t port)
{
    int sockfd = socket(AF_INET, SOCK_STREAM, 0);
    if (sockfd < 0) {
        printf("[!] failed to create socket\n");
        return -1;
    }

    struct sockaddr_in servaddr;
    memset((char *)&servaddr, 0, sizeof(servaddr));
    servaddr.sin_family = AF_INET;
    // listen on all available interfaces, i.e. 0.0.0.0
    servaddr.sin_addr.s_addr = htonl(INADDR_ANY);
    servaddr.sin_port = htons(port);

    // attempts to bind the socket to the IP address on the specified port
    if (bind(sockfd, (struct sockaddr *)&servaddr, sizeof(servaddr)) < 0) {
        printf("[!] failed to bind to socket. Err: [%d]\n", errno);
        close(sockfd);
        return -1;
    }

    // try and start listening with our socket
    if (listen(sockfd, SOMAXCONN) < 0) {
        printf("[!] failed to listen. Err: [%d]\n", errno);
        close(sockfd);
        return -1;
    }
    return sockfd;
}

int accept_client(struct client* client, struct server *server) 
{
    socklen_t c = sizeof(client->client_ip);
    if(-1 == (client->sockfd = accept(server->sockfd, (void*)&client->client_ip, &c))) {
        // we failed to accept, return so
        return -1;
    }
    return 0;
}

int socket_write(int sockfd, char *msg, int msg_size)
{
    write(sockfd, msg, msg_size);
}
