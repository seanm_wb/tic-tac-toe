#include <stdio.h>
#include "../include/battleship.h"

//void* play_battleship(void *arg)
int play_battleship_main()
{
    char gameboard[8][10] = { 0 };
    int i = 0;
    int j = 0;

    // generate board
    for(i = 65; i < 73; i++) {
        gameboard[(i-65)][0] = (char)i;
        for(j = 1; j < 10; j++) {
            gameboard[(i-65)][j] = j+'0';
        }
    }

    print_battleship_board(gameboard);
    

    return 0;
}

/*
 *  prints the gameboard
 */
void print_battleship_board(char (*gameboard)[10])
{
    int i = 0;
    int j = 0;
    for(i=65; i< 73; i++) {
        printf("%c ", gameboard[(i-65)][0]);
        for(j=1; j<10;j++) {
            printf("%c ", gameboard[(i-65)][j]);
        }
        printf("\n");
    }
}
