#ifndef __TIC_TAC_TOE_H__
#define __TIC_TAC_TOE_H__



void* play_tic_tac_toe(void *arg);
void* play_battleship(void *arg);
int check_board(int sockfd, char (*gameboard)[3]);
int check_win(int sockfd, char (*gameboard)[3]);
void print_board(int sockfd, char (*gameboard)[3]);
void get_ai_tile(int sockfd, char *ai_tile, char (*gameboard)[3]);
void get_user_tile(int sockfd, char *user_tile, char (*gameboard)[3]);
#endif
